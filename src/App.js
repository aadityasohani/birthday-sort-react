
import './App.css';
import Birthdays from './components/Birthdays';
import { useState } from 'react';


function App() {
  const [shuffling,setShuffling] = useState(false);
  return (
    <div className="App">
      <Birthdays shuffling = {shuffling}/>
      <button onClick={(e)=> setShuffling(!shuffling)}>shuffle</button>
    </div>
  );
}

export default App;
