import React, { useState,useEffect } from 'react';
import './Birthdays.css';

export default function Birthdays(props) {
    const [data,setData] = useState([]);
    const [type,setType] = useState("random");
    const [styClass,setStyClass] = useState("random");
    const url = "https://bitbucket.org/aadityasohani/birthday-sort/raw/863a53f8b643f8972a9793b4b1945dc7e9377154/birthday.json";


    useEffect(()=>{
        var shuffle = () =>{
            const arr = [...data];
            for(let i = arr.length-1;i>0;i--){
                var j = Math.floor(Math.random()*i);
                var x = arr[i];
                arr[i]= arr[j];
                arr[j]=x;
            }

            setData(arr);
        }
        if(props.shuffling){
            setStyClass("random")
            shuffle();
        }
    },[props.shuffling]);


    

    const getData=()=>{
        fetch(url).then((d)=>{
            return d.json();
        }).then((actData)=>{
            return setData(actData);
        })
    }

    const compNameAsc = (x,y)=>{
        if(x.name<=y.name){
            return 1;
        }
        return -1;
    }
    const compNameDec = (x,y)=>{
        if(x.name>y.name){
            return 1;
        }
        return -1;
    }
    const compDobAsc = (d1,d2)=>{
        var dt1= new Date(d1.dob.slice(6)+"/"+d1.dob.slice(3,5)+"/"+d1.dob.slice(0,2));
        var dt2= new Date(d2.dob.slice(6)+"/"+d2.dob.slice(3,5)+"/"+d2.dob.slice(0,2));
        if(dt1<dt2){
            return 1;
        }else{
            return -1;  
        }
    }
    const compDobDec = (d1,d2)=>{
        var dt1= new Date(d1.dob.slice(6)+"/"+d1.dob.slice(3,5)+"/"+d1.dob.slice(0,2));
        var dt2= new Date(d2.dob.slice(6)+"/"+d2.dob.slice(3,5)+"/"+d2.dob.slice(0,2));
        if(dt1>=dt2){
            return 1;
        }else{
            return -1;  
        }
    }

    const sortByName= (x) =>{
        var arr = [...data] ;
        if(x) arr = arr.sort(compNameAsc);
        else arr = arr.sort(compNameDec);
        var s = x?" Ascending":" Descending";
        setStyClass("name");
        setType("Name"+s);
        setData(arr);
    }


    const sortByDOB = (x)=>{
        var arr = [...data];
        if(x) arr = arr.sort(compDobAsc);
        else arr = arr.sort(compDobDec);
        var s = x?" Ascending":" Descending";
        setStyClass("dob")
        setType("DOB"+s);
        setData(arr);
    }



    useEffect(() => {
      getData();
    }, []);
    

  return (<div>
      <h1>Birthdays woohooo!!!!</h1>
      <h1>
              Sorting type : {type}
      </h1>
      <table>
          
      <tbody>

          <tr className={styClass}>
              <th>
                  <span onClick={(e)=>sortByName(false)}>&#9650;</span >
                    Name
                  <span onClick={(e)=>sortByName(true)}>&#9660;</span>
              </th>
              <th>
                <span onClick={(e)=>sortByDOB(false)}>&#9650;</span >
                    DOB
                <span onClick={(e)=>sortByDOB(true)}>&#9660;</span>
              </th>
          </tr>
            {data.map((d)=>{
                return(<tr key = {d.name}>
                    <td>{d.name}</td>
                    <td>{d.dob}</td>
                </tr>)
             })}     
        </tbody>
      </table>
  </div>);
}
